import numpy as np
import cv2

cap = cv2.VideoCapture(0)

while(True):
	#capture frame by frame
	ret, frame =cap.read() 
	
	#Displaying the resulting frame
	cv2.imshow("frame", frame)
	if cv2.waitKey(20) & 0xff == ord('q'):
		break
# release the capture
cap.release()
cv2.destryAllWindows()